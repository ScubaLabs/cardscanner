using UnityEngine;
using System.Collections.Generic;
using Vuforia;
using System;

[RequireComponent(typeof (VuforiaBehaviour))]
public class SmoothCamera : MonoBehaviour
{

    public Transform target;

    public float smoothTime = 0.3F;

    public Vector3 velocity = Vector3.zero;

    public GameObject imageTargetObj;

    public float xOffset = 0f;

    public float yOffset = 0f;

    public float zOffset = 0f;

    // Use this for initialization

    void Start()
    {

        imageTargetObj = GameObject.Find("ImageTarget");

        target = imageTargetObj.transform;

    }



    // Update is called once per frame

    void Update()
    {

        

    }
    private void LateUpdate()
    {
        target = imageTargetObj.transform;

        Vector3 targetPosition = new Vector3((float)Math.Round(target.position.x + xOffset, 2),

                                                                    (float)Math.Round(target.position.y + yOffset, 2),

                                                                    (float)Math.Round(target.position.z + zOffset, 2));



        transform.parent.transform.position = targetPosition;
    }
}