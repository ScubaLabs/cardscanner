﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    private GameObject currentModel;
    public List<GameObject> obj = new List<GameObject>();
    int modelIndex = 0;
    public GameObject leftButton, rightButton;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void RightButton()
    {
        deactivateObjects();

        if (modelIndex == obj.Count - 1)
            modelIndex = -1;

        modelIndex++;

        currentModel = obj[modelIndex];
        currentModel.SetActive(true);
    }
    public void LeftButton()
    {
        deactivateObjects();

        if (modelIndex == 0)
            modelIndex = obj.Count;

        modelIndex--;

        currentModel = obj[modelIndex];
        currentModel.SetActive(true);
    }
    public void deactivateObjects()
    {
        if (currentModel)
            currentModel.SetActive(false);
    }
    public void LoadFirstModel()
    {
        deactivateObjects();
        currentModel = obj[modelIndex];
        currentModel.SetActive(true);
    }
    public void deActivateButtons()
    {
        leftButton.SetActive(false);
        rightButton.SetActive(false);
    }
    public void ActivateButtons()
    {
        leftButton.SetActive(true);
        rightButton.SetActive(true);
    }
}
