﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;


public class ErrorMessage : MonoBehaviour {

    #region Script's Summary
    //This script is displaying flash messages
    #endregion


    #region Variable Declaration
    //--------------------Variable Declaration-----------------------------

    public static string _message = "";

    private bool _isRunning = false;

    //----------------------------------------------------------------------
    #endregion

    #region Unity's method
    //----------------------Unity's Method----------------------------------

    /*
	void Awake (){

	}
	*/

    /*void Start()
    {

        //--------- variable definitions -----------



        //-------------------------------------------
    }*/

    private void Update()
    {
        if (_message != "")
            Show();
    }

    //----------------------------------------------------------------------
    #endregion

    #region User's Functions
    //----------------------User's Functions--------------------------------

    /// <summary>
    /// display the flash message
    /// </summary>
    private void Show()
    {
        if (_isRunning)
            return;

        _isRunning = true;

        transform.GetChild(0).gameObject.SetActive(true);
        GetComponentInChildren<Text>().text = _message;
        _message = "";
        StartCoroutine(FadeOut());
    }

    /// <summary>
    /// fade the image out
    /// </summary>
    IEnumerator FadeOut()
    {
        Image _flashImage = GetComponentInChildren<Image>();
        Color _startCol = _flashImage.color;
        Color _endCol = new Color(_flashImage.color.r, _flashImage.color.g, _flashImage.color.b, 0f);

        // Execute this loop once per frame until the timer exceeds the duration.
        float _timer = 0f;
        float _fadeDuration = 1f;
        while (_timer <= _fadeDuration)
        {
            // Set the colour based on the normalised time.
            _flashImage.color = Color.Lerp(_startCol, _endCol, _timer / _fadeDuration);

            // Increment the timer by the time between frames and return next frame.
            _timer += Time.deltaTime;
            yield return null;
        }

        _flashImage.color = new Color(_startCol.r, _startCol.g, _startCol.b, 1f);
        _flashImage.gameObject.SetActive(false);
        _isRunning = false;
    }
    
    //----------------------------------------------------------------------

    #endregion
}
