﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class SwipeScript : MonoBehaviour {

    private Touch initialTouch = new Touch();
    private float distance;
    private bool hasSwiped = false;
    private string swipeValue;
    private string[] modelsName = new string[3];
    private GameObject previousModel;

    int modelIndex = 1;
    
    void Start () {
        modelsName[0] = "model2";
        modelsName[1] = "model3";
        modelsName[2] = "model4";
	}
	
	// Update is called once per frame
	
     
    void FixedUpdate()
    {
        foreach(Touch t in Input.touches)
        {
            if(t.phase == TouchPhase.Began)
            {
                initialTouch = t;
            }

            else if(t.phase == TouchPhase.Moved && !hasSwiped)
            {
                //distance moved
                float deltaX = initialTouch.position.x - t.position.x;
                float deltaY = initialTouch.position.y - t.position.y;
                distance = Mathf.Abs(Mathf.Sqrt((deltaX * deltaX) + (deltaY * deltaY)));
                bool swipedSideways = Mathf.Abs(deltaX) > Mathf.Abs(deltaY);
               // swipeValue = "distance" + distance;
                if(distance > 150f)
                {
                    if (swipedSideways && deltaX > 0)//swiped left
                    {
                        if (modelIndex > 0)
                        {
                            modelIndex--;

                            
                            string name = modelsName[modelIndex];
                            swipeValue = name;
                            destroyObject();
                            Debug.Log(swipeValue);
                            previousModel = Instantiate(Resources.Load("_Prefabs/"+name, typeof(GameObject)) as GameObject);
                        }
                        

                    }else if(swipedSideways && deltaX <= 0)//swiped right
                    {
                        if (modelIndex < modelsName.Length)
                        {
                            
                            string name = modelsName[modelIndex];
                            swipeValue = name;
                            destroyObject();
                            modelIndex++;

                            previousModel = Instantiate(Resources.Load("_Prefabs/"+name, typeof(GameObject)) as GameObject);
                        }
                    }
                    hasSwiped = true;
                }

            }else if(t.phase == TouchPhase.Ended)
            {
                initialTouch = new Touch();
                hasSwiped = false;
            }
        }
    }
    private void destroyObject()
    {
        if (previousModel!=null)
            Destroy(previousModel);
    }
}
